# Changelog
All changes to the **Intersect Queue** will be documented in this file.


## [1.0.0] - TBD
- Added main `QueueManager` class and `QueryProcessor` interface